//
//  AppDelegate.h
//  DemoViewController
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
