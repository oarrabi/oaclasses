//
//  NSString+OAAddition.m
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "NSString+OAAddition.h"

@implementation NSString (OAAddition)

- (NSString*)emptyStringIfNil
{
    if(self)
        return @"";
    
    return self;
}

@end
