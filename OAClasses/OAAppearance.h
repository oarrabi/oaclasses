//
//  OAAperance.h
//  OAClasses
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAAppearance : NSObject

+ (id) appearanceForClass:(Class)thisClass;

@end
