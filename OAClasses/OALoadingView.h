//
//  OALoadingView.h
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OALoadingDefines.h"

@interface OALoadingView : UIView

+ (void)attachToView:(UIView*)view withStyle:(OALoadingViewStyle)loadingStyle
               color:(UIColor*)color;

+ (void)attachToView:(UIView*)view;

@end