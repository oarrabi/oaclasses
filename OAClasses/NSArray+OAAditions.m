//
//  NSArray+OAAditions.m
//  Neqaty
//
//  Created by Omar on 12/4/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "NSArray+OAAditions.h"

@implementation NSArray (OAAditions)

- (BOOL)empty
{
    return self.count == 0;
}

@end

@implementation NSMutableArray (OAAditions)

- (BOOL)empty
{
    return self.count == 0;
}

@end
