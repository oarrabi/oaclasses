//
//  OAAlertView.m
//  OAClasses
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OAHudManager.h"
#import "MRProgress.h"
#import "DejalActivityView.h"
#import "OAAppearance.h"

#define DEJAIL_ACTIVITY_VIEW 324563245
NSMutableDictionary *dictionary;
@implementation OAHudManager

+ (void)initialize
{
    dictionary = [NSMutableDictionary new];
}

+ (void) show
{
    [self showInView:nil withText:NSLocalizedString(@"Loading ...", nil)];
}

+ (void) showWithText:(NSString*)string;
{
    [self showInView:nil withText:string];
}

+ (void) showInView:(UIView*)view
{
    [self showInView:view withText:NSLocalizedString(@"Loading ...", nil)];
}

+ (void) showInView:(UIView*)view withText:(NSString*)string
{
    if (!view) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    if (!view) {
        view = [[[UIApplication sharedApplication] delegate] window];
    }
    
    [self hideFromView:view];
    
    if (isiOS7OrHigher())
    {
        MRProgressOverlayView *progressView = [MRProgressOverlayView
                                               showOverlayAddedTo:view animated:YES];
        progressView.mode = MRProgressOverlayViewModeIndeterminateSmall;
        progressView.titleLabelText = string;
        progressView.titleLabel.font = self.font;
        progressView.titleLabel.textColor = self.color;
        progressView.tintColor = self.tintColor;
    }
    else
    {
        DejalBezelActivityView *dejailView = [[DejalBezelActivityView alloc] initForView:view
                                                                               withLabel:string
                                                                                   width:0];
        [dejailView animateShow];
        dejailView.activityLabel.font = OAHudManager.font;
        dejailView.tag = DEJAIL_ACTIVITY_VIEW;
    }
}

+ (void) alertWithText:(NSString*)string
{
    [self showInView:nil withText:string
             success:NO withButton:NSLocalizedString(@"Ok", nil)];
}

+ (void) showError:(NSError*)error
{
    [self showInView:Nil withError:error];
}

+ (void) showError:(NSError*)error withButton:(NSString*)button
{
    [self showInView:nil withText:error.localizedDescription
             success:NO withButton:button];
}

+ (void) showInView:(UIView*)view withError:(NSError*)error
{
    [self showInView:view withText:error.localizedDescription success:NO withButton:NSLocalizedString(@"Ok", nil)];
}

+ (void) showInView:(UIView*)view withText:(NSString*)string success:(BOOL)success
         withButton:(NSString*)button
{
    if (!view) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    [self hideFromView:view];
    
    if (button)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:string
                                                           delegate:nil cancelButtonTitle:button otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (isiOS7OrHigher())
    {
        MRProgressOverlayView *progressView = [MRProgressOverlayView
                                               showOverlayAddedTo:view animated:YES];
        progressView.mode = success?MRProgressOverlayViewModeCheckmark : MRProgressOverlayViewModeCross;
        progressView.titleLabelText = string;
        progressView.titleLabel.font = OAHudManager.font;
        progressView.titleLabel.textColor = OAHudManager.color;
        progressView.tintColor = self.tintColor;
    }
    else
    {
        DejalBezelActivityView *dejailView = [[DejalBezelActivityView alloc] initForView:view
                                                                               withLabel:string
                                                                                   width:0];
        [dejailView animateShow];
        dejailView.activityLabel.font = OAHudManager.font;
        dejailView.tag = DEJAIL_ACTIVITY_VIEW;
    }
    
    [self performSelector:@selector(hideFromView:) withObject:view afterDelay:2];
}

+ (void) hide
{
    [self hideFromView:nil];
}

+ (void) hideFromView:(UIView*)view
{
    if (!view) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    if (isiOS7OrHigher())
    {
        [MRProgressOverlayView dismissOverlayForView:view animated:YES];
    }
    else
    {
        DejalBezelActivityView *dejailView = (DejalBezelActivityView *)[view viewWithTag:DEJAIL_ACTIVITY_VIEW];
        [dejailView animateRemove];
        [dejailView performSelector:@selector(removeFromSuperview)
                         withObject:nil afterDelay:1];
    }
}

BOOL isiOS7OrHigher()
{
    return floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1;
}

+ (UIFont*)font
{
    return dictionary[@"font"];
}

+ (void)setFont:(UIFont*)font
{
    dictionary[@"font"] = font;
}

+ (UIColor*)color
{
    return dictionary[@"color"];
}

+ (void)setColor:(UIColor*)color
{
    dictionary[@"color"] = color;
}

+ (UIColor*)tintColor
{
   return dictionary[@"tintColor"];
}

+ (void)setTintColor:(UIColor*)tintColor
{
   dictionary[@"tintColor"] = tintColor;
}
@end
