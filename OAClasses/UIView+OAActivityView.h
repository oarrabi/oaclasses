//
//  UIView+OALoadingView.h
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OALoadingDefines.h"

@interface UIView (OALoadingView)

@property (nonatomic, assign, readonly) BOOL isLoading;

- (void)startActivity;
- (void)startActivityWithStyle:(OALoadingViewStyle)loadingStyle
                         color:(UIColor*)color;

- (void)stopActivity;

- (void)stopActivityWithMessage:(NSString*)message;

- (void)stopActivityWithMessage:(NSString*)message
                    reloadBlock:(void (^)(void))reloadBlock;

- (void)stopActivityWithMessage:(NSString*)text
                           font:(UIFont*)font
                          color:(UIColor*)color
                    reloadBlock:(void (^)(void))reloadBlock;
@end
