//
//  OALoadingDefines.h
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    OALoadingViewStyleDefault,
    OALoadingViewStyleLarge,
}OALoadingViewStyle;

