//
//  OAInformableAlertManager.m
//  Neqaty
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OAInformableAlertManager.h"

@implementation OAInformableAlertManager
{
    __weak UIView *_view;
    __weak NSString *_text;
}

- (id)initWithView:(UIView*)view text:(NSString*)text
{
    self = [super init];
    if (self) {
        _view = view;
        _text = text;
    }
    return self;
}

- (void)requestStarted
{
    [OAHudManager showInView:_view withText:_text];
}

- (void)requestSucceeded
{
    [OAHudManager hideFromView:_view];
}

- (void)requestFailed:(NSError *)error
{
    [OAHudManager showInView:_view withText:_text success:NO withButton:NSLocalizedString(@"Ok", nil)];
}
@end
