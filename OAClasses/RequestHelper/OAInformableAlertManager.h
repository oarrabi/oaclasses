//
//  OAInformableAlertManager.h
//  Neqaty
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OAClasses.h"
#import "RequestHelper.h"

@interface OAInformableAlertManager : OAHudManager<RequestHelperInformable>

- (id)initWithView:(UIView*)view text:(NSString*)text;

@end
