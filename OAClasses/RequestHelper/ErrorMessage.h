//
//  ErrorMessage.h
//  Neqaty
//
//  Created by Omar on 11/26/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "BaseModel.h"

@interface ErrorMessage : NSError

+ (ErrorMessage*)errorMessageForDictionary:(NSDictionary*)dictionary;

@end
