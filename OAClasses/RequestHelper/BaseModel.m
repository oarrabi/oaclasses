//
//  BaseModel.m
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "BaseModel.h"
//#import "SettingsManager.h"

@implementation BaseModel
//
//- (void)forwardInvocation:(NSInvocation *)anInvocation
//{
//    SEL aSelector = anInvocation.selector;
//    NSString *selector = NSStringFromSelector(aSelector);
//    selector = [NSString stringWithFormat:@"%@%@", selector,
//                [SettingsManager isArabic]? @"Ar" : @"En"];
//    
//    SEL newSelector = NSSelectorFromString(selector);
//    
//    NSString *str = [self performSelector:newSelector];
//    if (str == nil) {
//        newSelector = @selector(empty);
//    }
//    
//    anInvocation.selector = newSelector;
//    [anInvocation invokeWithTarget:self];
//}
//
//- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
//{
//    NSString *selector = NSStringFromSelector(aSelector);
//    selector = [NSString stringWithFormat:@"%@%@", selector,
//                [SettingsManager isArabic]? @"Ar" : @"En"];
//    
//    SEL newSelector = NSSelectorFromString(selector);
//    
//    NSMethodSignature *methodSig = [[self class] instanceMethodSignatureForSelector:newSelector];
//    
//    if (methodSig) {
//        NSString *str = [self performSelector:newSelector];
//        if (str == nil) {
//            methodSig = [[self class] instanceMethodSignatureForSelector:@selector(empty)];
//        }
//    }
//    
//    return methodSig;
//}

- (NSString*)empty
{
    return @"";
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{};
}

//- (id)performSelector:(SEL)aSelector
//{
//    return [super performSelector:aSelector];
//}

@end
