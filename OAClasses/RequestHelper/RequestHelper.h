//
//  RequestHelper.h
//  RoyalGuards
//
//  Created by IMac on 11/29/12.
//  Copyright (c) 2012 IMac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void (^RequestArrayBlock)(NSArray* result);
typedef void (^RequestIntBlock)(int result);
typedef void (^RequestBoolBlock)(BOOL success);
typedef void (^RequestBoolMessageBlock)(BOOL success, NSString *message);
typedef void (^RequestIdBlock)(id result);
typedef void (^RequestFinishBlock)();

@protocol RequestHelperCancellable <NSObject>
- (void)cancel;
@end

@protocol RequestHelperInformable <NSObject>
- (void)requestStarted;
- (void)requestSucceeded;
- (void)requestFailed:(NSError*)error;
@end

@interface RequestHelper : NSObject

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                 isJsonResponse:(BOOL)isJsonResponse
                                         header:(NSDictionary*)header
                                        handler:(void (^)(id response, NSError * error))handler;

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                        handler:(void (^)(id response, NSError * error))handler;

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                         header:(NSDictionary*)header
                                        handler:(void (^)(id response, NSError * error))handler;

@end
