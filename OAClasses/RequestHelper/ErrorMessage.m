//
//  ErrorMessage.m
//  Neqaty
//
//  Created by Omar on 11/26/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "ErrorMessage.h"

static NSDictionary *errorDictionary;
@implementation ErrorMessage

+(void)initialize
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ErrorMessages" ofType:@"plist"];
    errorDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
}

+ (ErrorMessage*)errorMessageForDictionary:(NSDictionary*)dictionary
{
    if (![dictionary isKindOfClass:[NSDictionary class]] || !dictionary[@"errorCode"])
        return nil;
    
    int code = [dictionary[@"errorCode"] integerValue];
    NSString *localizedDescription = [self errorMessageForCode:code];
    
    if (!localizedDescription)
        localizedDescription = dictionary[@"errorMessage"];
    
    if (!localizedDescription || localizedDescription.length == 0)
        localizedDescription = [self errorMessageForCode:-100];
    
    ErrorMessage *message = [[ErrorMessage alloc]
                             initWithDomain:@"API"
                             code:code
                             userInfo:
  @{NSLocalizedDescriptionKey : localizedDescription}];
    
    return message;
}

+ (NSString*)errorMessageForCode:(int)code
{
    NSString *str = errorDictionary[[NSString stringWithFormat:@"%d", code]];
    return NSLocalizedString(str, nil);
}

@end
