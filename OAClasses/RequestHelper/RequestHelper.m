//
//  RequestHelper.m
//  RoyalGuards
//
//  Created by IMac on 11/29/12.
//  Copyright (c) 2012 IMac. All rights reserved.
//

#import "RequestHelper.h"
#import "ErrorMessage.h"
#import "TTTURLRequestFormatter.h"

NSString *NOT_AUTORIZED_NOTIFICATION;
@implementation RequestHelper

+ (void)load
{
    NOT_AUTORIZED_NOTIFICATION = @"NOT_AUTORIZED_NOTIFICATION";
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                 isJsonResponse:(BOOL)isJsonResponse
                                         header:(NSDictionary*)header
                                        handler:(void (^)(id response, NSError * error))handler
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:nil
                      listeners:nil
                     isJsonPost:isJson
                 isJsonResponse:isJsonResponse
                         header:header
                   successBlock:^(id response) {
                       handler(response, nil);
                   } failureBlock:^(NSError *error) {
                       handler(nil, error);
                   }];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                         header:(NSDictionary*)header
                                        handler:(void (^)(id response, NSError * error))handler
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:nil
                      listeners:nil
                     isJsonPost:isJson
                         header:header
                   successBlock:^(id response) {
                       handler(response, nil);
                   } failureBlock:^(NSError *error) {
                       handler(nil, error);
                   }];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(id)params
                                         method:(NSString*)method
                                     isJsonPost:(BOOL)isJson
                                        handler:(void (^)(id response, NSError * error))handler
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:nil
                      listeners:nil
                     isJsonPost:isJson
                         header:nil
                   successBlock:^(id response) {
                       handler(response, nil);
                   } failureBlock:^(NSError *error) {
                       handler(nil, error);
                   }];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(NSDictionary*)params
                                         method:(NSString*)method
                                    accessToken:(NSString*)accessToken
                                   successBlock:(void (^)(id response))success
                                   failureBlock:(void (^)(NSError * error))failure
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:accessToken
                      listeners:nil
                   successBlock:success
                   failureBlock:failure];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(NSDictionary*)params
                                         method:(NSString*)method
                                    accessToken:(NSString*)accessToken
                                      listeners:(NSArray*)listeners
                                   successBlock:(void (^)(id response))success
                                   failureBlock:(void (^)(NSError * error))failure
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:accessToken
                      listeners:listeners
                     isJsonPost:NO
                         header:nil
                   successBlock:success
                   failureBlock:failure];
}


+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(NSDictionary*)params
                                         method:(NSString*)method
                                    accessToken:(NSString*)accessToken
                                      listeners:(NSArray*)listeners
                                     isJsonPost:(BOOL)isJson
                                         header:(NSDictionary*)header
                                   successBlock:(void (^)(id response))success
                                   failureBlock:(void (^)(NSError * error))failure
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:accessToken
                      listeners:listeners
                     isJsonPost:isJson
                 isJsonResponse:YES
                         header:header
                   successBlock:success
                   failureBlock:failure];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                         params:(NSDictionary*)params
                                         method:(NSString*)method
                                    accessToken:(NSString*)accessToken
                                      listeners:(NSArray*)listeners
                                     isJsonPost:(BOOL)isJson
                                 isJsonResponse:(BOOL)isJsonResponse
                                         header:(NSDictionary*)header
                                   successBlock:(void (^)(id response))success
                                   failureBlock:(void (^)(NSError * error))failure
{
    [self logRequestWithUrl:stringUrl params:params];
    
    AFHTTPRequestSerializer *requestSerializer;
    
    if (isJson)
        requestSerializer = [AFJSONRequestSerializer serializer];
    else
        requestSerializer = [AFHTTPRequestSerializer serializer];
    
    NSMutableURLRequest *request = [requestSerializer
                                    requestWithMethod:method? method : @"GET"
                                    URLString:stringUrl
                                    parameters:params
                                    error:nil];
    
    if (header)
    {
        [header enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [request setValue:obj forHTTPHeaderField:key];
        }];
    }

    if (accessToken)
        [request setValue:[NSString stringWithFormat:@"bearer %@", accessToken] forHTTPHeaderField:@"Authorization"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    if (isJsonResponse)
        operation.responseSerializer = [AFJSONResponseSerializer new];
    else
        operation.responseSerializer = [AFHTTPResponseSerializer new];

    request.timeoutInterval = 30.0;
    
    NSLog(@"REQUEST ************ %@\n************", [TTTURLRequestFormatter cURLCommandFromURLRequest:request]);
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ErrorMessage *errorMessage = [ErrorMessage errorMessageForDictionary:responseObject];
        
        if (errorMessage.code == 0) {
            [RequestHelper handleSuccess:operation
                          responseObject:responseObject
                            successBlock:success];
        }
        else
        {
            [RequestHelper handleFailure:operation
                                   error:errorMessage
                            failureBlock:failure];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (operation.response.statusCode == 200)
        {
            [RequestHelper handleSuccess:operation
                          responseObject:nil
                            successBlock:success];
        }
        else
        {
            [RequestHelper handleFailure:operation
                                   error:error
                            failureBlock:failure];
        }
    }];
    
    [operation start];
    return (id<RequestHelperCancellable>)operation;
}

+ (void)informStart:(NSArray*)array
{
    [array enumerateObjectsUsingBlock:^(id<RequestHelperInformable> obj, NSUInteger idx, BOOL *stop) {
        [obj requestStarted];
    }];
}

+ (void)informSuccess:(NSArray*)array
{
    [array enumerateObjectsUsingBlock:^(id<RequestHelperInformable> obj, NSUInteger idx, BOOL *stop) {
        [obj requestSucceeded];
    }];
}

+ (void)informFailure:(NSArray*)array error:(NSError*)error
{
    [array enumerateObjectsUsingBlock:^(id<RequestHelperInformable> obj, NSUInteger idx, BOOL *stop) {
        [obj requestFailed:error];
    }];
}

+ (id<RequestHelperCancellable>) requestWithUrl:(NSString*)stringUrl
                                    params:(NSDictionary*)params
                                    method:(NSString*)method
                              successBlock:(void (^)(id response))success
                              failureBlock:(void (^)(NSError * error))failure
{
    return [self requestWithUrl:stringUrl
                         params:params
                         method:method
                    accessToken:nil
                   successBlock:success
                   failureBlock:failure];
}

+ (void)logRequestWithUrl:(NSString*)stringUrl
                  params:(id)params
{
    if ([params isKindOfClass:[NSDictionary class]])
    {
        NSString *completeRequest = stringUrl;
        NSString *parameterString = @"";
        for (id key in [params allKeys]) {
            parameterString = [ NSString stringWithFormat:@"%@=%@&%@",key,[params objectForKey:key],parameterString ];
            NSLog(@"%@=%@",key,[params objectForKey:key]);
        }
        completeRequest = [NSString stringWithFormat:@"%@?%@",completeRequest,parameterString];
        NSLog(@"Request : %@",completeRequest);
    }
    else
    {
        NSString *completeRequest = stringUrl;
        NSString *parameterString = @"";
        NSLog(@"body=%@",params);
        
        completeRequest = [NSString stringWithFormat:@"%@?%@",completeRequest,parameterString];
        NSLog(@"Request : %@",completeRequest);
    }
}

+ (NSString *)urlencode:(NSString*)str
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)str,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
}

+ (void) handleSuccess:(AFHTTPRequestOperation *) operation
        responseObject:(id)responseObject
          successBlock:(void (^)(NSDictionary * response))success
{
    if(success)
    {
        success(responseObject);
    }
}

+ (void) handleStringSuccess:(AFHTTPRequestOperation *) operation
              responseObject:(id)responseObject
                successBlock:(void (^)(NSString * response))success
{
    if(success)
    {
        success(responseObject);
    }
}

+ (void) handleFailure:(AFHTTPRequestOperation *) operation
                 error:(NSError *) error
          failureBlock:(void (^)(NSError * error))failure
{
    if (operation.isCancelled)
    {
        NSLog(@"Request cancelled %@", operation.request.URL);
        return;
    }
    
    if (operation.responseString)
    {
        NSDictionary *dic = [NSJSONSerialization
                             JSONObjectWithData:operation.responseData
                             options:NSJSONReadingMutableContainers error:nil];
        
        ErrorMessage *errorMessage = [ErrorMessage errorMessageForDictionary:dic];
        
        if (errorMessage)
        {
            if (errorMessage.code != 0)
            {
                error = errorMessage;
            }
        }
        else
        {
            NSHTTPURLResponse *urlResponse = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
            if(urlResponse && (urlResponse.statusCode == 401 || urlResponse.statusCode == 403))
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:NOT_AUTORIZED_NOTIFICATION
                                                                    object:nil];
            }
        }
    }
    
    if(failure)
    {   
        if(!failure)
        {
            UIAlertView *view = [[UIAlertView alloc] initWithTitle:@""
                                                           message:error.localizedDescription
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [view show];
        }
        else
        {
            failure(error);
        }
    }
}

@end
