//
//  UIView+OALoadingView.m
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIView+OAActivityView.h"
#import <objc/runtime.h>
#import "OALoadingView.h"
#import "OAErrorReloadView.h"
#import "OAClasses.h"

extern char ISLOADING_PROPERTY;
int LOADING_VIEW_TAG;
int ERROR_RELOAD_VIEW_TAG;

@implementation UIView (OALoadingView)

+ (void)load
{
    LOADING_VIEW_TAG = 123321123;
    ERROR_RELOAD_VIEW_TAG = 1233223;
}

- (void)addRemoveLoadingViewIfIsLoading:(BOOL)isLoading
                              withStyle:(OALoadingViewStyle)loadingStyle
                                  color:(UIColor*)color
{
    OALoadingView *view = (OALoadingView*)[self viewWithTag:LOADING_VIEW_TAG];
    if (isLoading )
    {
        if(!view)
            [OALoadingView attachToView:self withStyle:loadingStyle color:color];
    }
    else
    {
        [view removeFromSuperview];
    }
    
    view.hidden = !isLoading;
}

- (BOOL)isLoading
{
    return [objc_getAssociatedObject(self, &ISLOADING_PROPERTY) integerValue];
}

- (void)startActivity
{
    [self startActivityWithStyle:OALoadingViewStyleDefault
                           color:nil];
}

- (void)startActivityWithStyle:(OALoadingViewStyle)loadingStyle
                         color:(UIColor*)color
{
    [self hideActivityView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self addRemoveLoadingViewIfIsLoading:YES
                                withStyle:loadingStyle
                                    color:color];
    
    [self willChangeValueForKey:@"isLoading"];
    objc_setAssociatedObject(self, &ISLOADING_PROPERTY, @(YES),
                             OBJC_ASSOCIATION_ASSIGN);
    [self didChangeValueForKey:@"isLoading"];
}

- (void)stopActivity
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self addRemoveLoadingViewIfIsLoading:NO
                                withStyle:OALoadingViewStyleDefault
                                    color:nil];
    
    [self willChangeValueForKey:@"isLoading"];
    objc_setAssociatedObject(self, &ISLOADING_PROPERTY, @(NO),
                             OBJC_ASSOCIATION_ASSIGN);
    [self didChangeValueForKey:@"isLoading"];
    [self hideActivityView];
}

- (void)stopActivityWithMessage:(NSString*)message
{
    [self stopActivityWithMessage:message reloadBlock:nil];
}

- (void)stopActivityWithMessage:(NSString*)message
                    reloadBlock:(void (^)(void))reloadBlock
{
    [self stopActivity];
    [OAErrorReloadView showInView:self
                      withMessage:message
                      reloadBlock:reloadBlock];
}

- (void)stopActivityWithMessage:(NSString*)text
                           font:(UIFont*)font
                          color:(UIColor*)color
                    reloadBlock:(void (^)(void))reloadBlock
{
    [self stopActivity];
    [OAErrorReloadView showInView:self
                      withMessage:text
                             font:font
                            color:color
                      reloadBlock:reloadBlock];
}

- (void)hideActivityView
{
    OAErrorReloadView *view = (OAErrorReloadView*)[self viewWithTag:ERROR_RELOAD_VIEW_TAG];
    [view removeFromSuperview];
}

@end
