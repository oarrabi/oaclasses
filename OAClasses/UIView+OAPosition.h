//
//  UIView+OAPosition.h
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (OAPosition)

+ (UIView*)viewPositionedInCenterOf:(UIView*)superView;

- (void)fillSuperView;
- (void)addSubviewAndPositionInCenter:(UIView *)view;
@end
