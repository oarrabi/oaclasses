//
//  UIButton+OALoadingView.m
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIButton+OAActivityView.h"
#import <objc/runtime.h>
#import "OAClasses.h"

extern char ISLOADING_PROPERTY;
extern int LOADING_VIEW_TAG;

@implementation UIButton (OALoadingView)

- (void)startActivityWithStyle:(OALoadingViewStyle)loadingStyle
                         color:(UIColor*)color
{
    [self adjustButtonForLoadingStat:YES];
    [super startActivityWithStyle:loadingStyle color:color];
}

- (void)stopActivity
{
    [self adjustButtonForLoadingStat:NO];
    [super stopActivity];
}

- (void)adjustButtonForLoadingStat:(BOOL)isLoading
{
    self.imageEdgeInsets =  isLoading ? UIEdgeInsetsMake(100, 100, 0, 0) :
    UIEdgeInsetsMake(0, 0, 0, 0);
    self.userInteractionEnabled = !isLoading;
}

@end
