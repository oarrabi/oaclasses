//
//  NSString+OAAddition.h
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OAAddition)

- (NSString*)emptyStringIfNil;

@end
