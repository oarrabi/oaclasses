//
//  UIAlertView+OAAddition.m
//  Neqaty
//
//  Created by Omar on 12/4/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIAlertView+OAAddition.h"

@implementation UIAlertView (OAAddition)

+ (void)showError:(NSError*)error
{
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil
                                                   message:error.localizedDescription
                                                  delegate:nil
                                         cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                         otherButtonTitles:nil];
    [view show];
}

@end
