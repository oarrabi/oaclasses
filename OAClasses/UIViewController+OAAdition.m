//
//  UIViewController+OAAdition.m
//  Neqaty
//
//  Created by Omar on 12/4/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIViewController+OAAdition.h"

@implementation UIViewController (OAAdition)

- (void)showErrorIfVisible:(NSError*)error
{
    if (self.isViewLoaded && self.view.window) {
        UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil
                                                       message:error.localizedDescription
                                                      delegate:nil
                                             cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                             otherButtonTitles:nil];
        [view show];
    }
}


@end
