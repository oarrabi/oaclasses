//
//  UIViewController+OAAdition.h
//  Neqaty
//
//  Created by Omar on 12/4/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (OAAdition)

- (void)showErrorIfVisible:(NSError*)error;

@end
