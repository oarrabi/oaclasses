//
//  OAErrorReloadView.m
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OAErrorReloadView.h"
#import "OAClasses.h"

static int ERROR_RELOAD_VIEW_TAG;
@implementation OAErrorReloadView
{
    __weak IBOutlet UILabel *errorLabel;
    __weak IBOutlet UIButton *button;
}

+ (void)load
{
    ERROR_RELOAD_VIEW_TAG = 1233223;
}

+ (OAErrorReloadView*)view
{
    OAErrorReloadView *view = (OAErrorReloadView *)[[[NSBundle mainBundle]
                                                     loadNibNamed:@"OAErrorReloadView" owner:nil options:nil] firstObject];
    
    return view;
}

+ (void)showInView:(UIView*)view
       withMessage:(NSString*)message
       reloadBlock:(void (^)(void))reloadBlock
{
    OAErrorReloadView *saLoadingView = [self addOrRetrieveOverlayViewFromView:view];
    saLoadingView.text = message;
    saLoadingView.textColor = [OAErrorReloadView appearance].textColor;
    saLoadingView.font = [OAErrorReloadView appearance].font;
    saLoadingView.reloadBlock = reloadBlock;
}

+ (void)showInView:(UIView*)view
       withMessage:(NSString*)message
              font:(UIFont*)font
             color:(UIColor*)color
       reloadBlock:(void (^)(void))reloadBlock
{
    OAErrorReloadView *saLoadingView = [self addOrRetrieveOverlayViewFromView:view];
    saLoadingView.text = message;
    saLoadingView.textColor = color? color : [OAErrorReloadView appearance].textColor;
    saLoadingView.font = font? font : [OAErrorReloadView appearance].font;
    saLoadingView.reloadBlock = reloadBlock;
}


+ (OAErrorReloadView*)addOrRetrieveOverlayViewFromView:(UIView*)view
{
    OAErrorReloadView *saLoadingView = (OAErrorReloadView*)[view viewWithTag:ERROR_RELOAD_VIEW_TAG];
    
    if (!saLoadingView) {
        saLoadingView = [OAErrorReloadView view];
        [view addSubviewAndPositionInCenter:saLoadingView];
        
        saLoadingView.tag = ERROR_RELOAD_VIEW_TAG;
    }
    
    return saLoadingView;
}

- (void)setText:(NSString *)text
{
    _text = text;
    errorLabel.text = text;
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    errorLabel.textColor = textColor;
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    errorLabel.font = font;
}

- (void)setReloadBlock:(void (^)(void))reloadBlock
{
    _reloadBlock = reloadBlock;
    if (!self.reloadBlock) {
        [button removeFromSuperview];
    }
    
}

- (IBAction)reloadTapped:(id)sender
{
    self.reloadBlock();
}

@end