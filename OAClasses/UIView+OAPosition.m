//
//  UIView+OAPosition.m
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIView+OAPosition.h"

@implementation UIView (OAPosition)

+ (UIView*)viewPositionedInCenterOf:(UIView*)superView
{
    UIView *view = [[UIView alloc] init];
    [view fillSuperView];
    return view;
}

- (void)fillSuperView
{
//    self.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.superview addConstraints:[UIView fillSuperViewConstraintsForView:self]];
    self.frame = self.superview.bounds;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.superview.backgroundColor = UIColor.redColor;
}

- (void)centerInSuperView
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self.superview addConstraints:[UIView centerInSuperViewConstraintsForView:self]];
    self.superview.backgroundColor = UIColor.redColor;
}

- (void)addSubviewAndPositionInCenter:(UIView *)view
{
    [self addSubview:view];
    [view fillSuperView];
}

+ (NSArray*)fillSuperViewConstraintsForView:(UIView*)view
{
    NSMutableArray *array = [@[] mutableCopy];
    [array addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"|[v]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"v": view}]];
    
    [array addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[v]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"v": view}]];
    
    return array;
}

+ (NSArray*)centerInSuperViewConstraintsForView:(UIView*)view
{
    NSMutableArray *array = [@[] mutableCopy];
    [array addObject:[NSLayoutConstraint constraintWithItem:view
                                                  attribute:NSLayoutAttributeCenterX
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:view.superview
                                                  attribute:NSLayoutAttributeCenterX
                                                 multiplier:1
                                                   constant:0]];
    
    [array addObject:[NSLayoutConstraint constraintWithItem:view
                                                  attribute:NSLayoutAttributeCenterY
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:view.superview
                                                  attribute:NSLayoutAttributeCenterY
                                                 multiplier:1
                                                   constant:0]];
    
    return array;
}

@end
