//
//  OALoadingView.m
//  Neqaty
//
//  Created by Omar on 11/28/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OALoadingView.h"
#import "OAClasses.h"

char ISLOADING_PROPERTY;
extern int LOADING_VIEW_TAG;
@interface OALoadingView()
@property (nonatomic, strong) UIView *observed;
@end

@implementation OALoadingView
{
    UIActivityIndicatorView *indicator;
}

+ (void)attachToView:(UIView*)view
{
    [self attachToView:view withStyle:OALoadingViewStyleDefault
                 color:[OALoadingView appearance].backgroundColor];
}

+ (void)attachToView:(UIView*)view withStyle:(OALoadingViewStyle)loadingStyle
               color:(UIColor*)color
{
    OALoadingView *loadingView = [[OALoadingView alloc] init];
    
    [loadingView addIndicatorViewStyle:loadingStyle color:color];
    loadingView.tag = LOADING_VIEW_TAG;
    loadingView.userInteractionEnabled = YES;
    loadingView.backgroundColor = [UIColor clearColor];
    
    loadingView.frame = view.bounds;
    loadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:loadingView];
    
    loadingView.observed = view;
    [view addObserver:loadingView
           forKeyPath:@"isLoading"
              options:NSKeyValueObservingOptionNew
              context:nil];
}

- (void)dealloc
{
    [self.observed removeObserver:self forKeyPath:@"isLoading"];
    self.observed = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    BOOL isLoading = [change[@"new"] boolValue];
    
    if (isLoading) {
        [indicator startAnimating];
        indicator.hidden = NO;
    }
    else
    {
        [indicator stopAnimating];
        indicator.hidden = YES;
    }
}

- (void)addIndicatorViewStyle:(OALoadingViewStyle)style
                        color:(UIColor*)color
{
    switch (style) {
        case OALoadingViewStyleDefault:
            indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            break;
            
        case OALoadingViewStyleLarge:
            indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            break;
            
        default:
            break;
    }
    
    if (color == nil) {
        color = [OALoadingView appearance].backgroundColor;
    }
    
    indicator.color = color;
    [self addSubview:indicator];
    [indicator fillSuperView];
    
    indicator.hidden = YES;
    indicator.userInteractionEnabled = NO;
}

@end