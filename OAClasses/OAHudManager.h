//
//  OAAlertView.h
//  OAClasses
//
//  Created by Omar on 12/5/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OAHudManager : NSObject

+ (void) show;
+ (void) showWithText:(NSString*)string;

+ (void) showInView:(UIView*)view;
+ (void) showInView:(UIView*)view withText:(NSString*)string;
+ (void) showInView:(UIView*)view withText:(NSString*)string success:(BOOL)success
         withButton:(NSString*)button;

//Error
+ (void) showError:(NSError*)error;
+ (void) showError:(NSError*)error withButton:(NSString*)button;
+ (void) showInView:(UIView*)view withError:(NSError*)error;

+ (void) hide;
+ (void) hideFromView:(UIView*)view;

//Alert
+ (void) alertWithText:(NSString*)string;

//Style
+ (UIFont*)font;
+ (void)setFont:(UIFont*)font;

+ (UIColor*)color;
+ (void)setColor:(UIColor*)color;

+ (UIColor*)tintColor;
+ (void)setTintColor:(UIColor*)tintColor;
@end
