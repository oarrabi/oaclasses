//
//  CommonUtilities.m
//  test1
//
//  Created by Omar on 11/12/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "CommonUtilities.h"

@implementation CommonUtilities

+ (BOOL) isiOS7OrHigher
{
    return floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1;
}

+ (UIImage *)imageForColor:(UIColor*)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void) addFullSizeLayoutConstraintsForView:(UIView*)innerView
                                      inView:(UIView*)outterView
{
    [outterView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[v]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"v": innerView}]];
    [outterView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[v]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"v": innerView}]];
    
    innerView.translatesAutoresizingMaskIntoConstraints = NO;
}

+ (BOOL)isArabic
{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    return [language isEqualToString:@"ar"] || [language isEqualToString:@"ara"];
}

BOOL isArabic()
{
    return [CommonUtilities isArabic];
}

@end
