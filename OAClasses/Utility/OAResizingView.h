//
//  OAResizingView.h
//  DrApp
//
//  Created by Omar on 12/21/13.
//  Copyright (c) 2013 InfusionApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OAResizingView : UIView

@property(nonatomic, strong) UIView *childView;

@end
