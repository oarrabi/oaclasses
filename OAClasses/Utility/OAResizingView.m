//
//  OAResizingView.m
//  DrApp
//
//  Created by Omar on 12/21/13.
//  Copyright (c) 2013 InfusionApps. All rights reserved.
//

#import "OAResizingView.h"

@implementation OAResizingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setChildView:(UIView *)childView
{
    _childView = childView;
    [childView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self addSubview:childView];
    
    [childView addObserver:self
                forKeyPath:@"frame"
                   options:NSKeyValueObservingOptionNew
                   context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self invalidateIntrinsicContentSize];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = self.childView.frame.size;
    size.height += 0;
    return size;
}

- (void)dealloc
{
    [self.childView removeObserver:self forKeyPath:@"frame"];
}

@end
