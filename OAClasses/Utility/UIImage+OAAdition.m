//
//  UIImage+OAAdition.m
//  MrContact
//
//  Created by Omar on 12/19/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "UIImage+OAAdition.h"

#if NSLOGGER_WAS_HERE
#import "NSLogger/LoggerClient.h"
#endif

@implementation UIImage (OAAdition)

- (UIImage *)imageScaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    float oldWidth = self.size.width;
    float oldheight = self.size.height;
    
    float widthScaleFactor = newSize.width / oldWidth;
    float heightScaleFactor = newSize.height / oldheight;
    
    float scaleFactor = MIN(widthScaleFactor, heightScaleFactor);
    
    float newHeight = oldheight * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [self drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSData*)imageData
{
    return UIImagePNGRepresentation(self);
}

- (void)logImage
{
#if NSLOGGER_WAS_HERE
    LogImageData(@"Image", 1, self.size.width, self.size.height, self.imageData);
#endif
}

- (void)logImageWithString:(NSString*)string
{
#if NSLOGGER_WAS_HERE
    LogImageData(string, 1, self.size.width, self.size.height, self.imageData);
#endif
}

+ (UIImage*)imageFromView:(UIView*)view
{
    UIGraphicsBeginImageContextWithOptions(view.frame.size, YES, 0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}
@end
