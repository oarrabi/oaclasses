//
//  UIColor+OAAdition.h
//  MrContact
//
//  Created by Omar on 12/13/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (OAAdition)

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIImage *)imageForHex:(NSString*)hex;

@end
