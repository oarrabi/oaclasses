//
//  OAKeyboardHelperView.m
//  Neqaty
//
//  Created by Omar on 11/26/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import "OAKeyboardHelperView.h"

@interface OAKeyboardHelperView ()
@property (nonatomic) CGFloat desiredHeight;

@end

@implementation OAKeyboardHelperView

- (void)awakeFromNib
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.shouldChangeSize = YES;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardRect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (self.shouldChangeSize)
        self.desiredHeight = CGRectGetHeight(keyboardRect);
    
    self.animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    self.animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    self.animationCurve = (self.animationCurve << 16);
    
    [self animateSizeChangeShow:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if (self.shouldChangeSize)
        self.desiredHeight = 0.0f;
    
    [self animateSizeChangeShow:NO];
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(UIViewNoIntrinsicMetric, self.desiredHeight);
}

- (void)animateSizeChangeShow:(BOOL)show
{
    [self invalidateIntrinsicContentSize];
    
    [UIView animateWithDuration:self.animationDuration
                          delay:0
                        options:self.animationCurve
                     animations:^{
                         
                         if (self.shouldChangeSize)
                         {
                             [self layoutIfNeeded];
                             [self.superview layoutIfNeeded];
                         }
                         
                         if ([self.delegat respondsToSelector:@selector(keyboardHelperView:willShowKeyboard:)]) {
                                [self.delegat keyboardHelperView:self
                                                willShowKeyboard:show];
                         }
                     }
                     completion:^(BOOL finished) {
                         
                         if ([self.delegat respondsToSelector:@selector(keyboardHelperView:didShowKeyboard:)]) {
                             [self.delegat keyboardHelperView:self
                                              didShowKeyboard:show];
                         }
                     }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
