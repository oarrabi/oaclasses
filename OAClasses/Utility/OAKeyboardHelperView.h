//
//  OAKeyboardHelperView.h
//  Neqaty
//
//  Created by Omar on 11/26/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OAKeyboardHelperView;
@protocol OAKeyboardHelperViewDelegate <NSObject>

@optional
- (void)keyboardHelperView:(OAKeyboardHelperView*)helperView
          willShowKeyboard:(BOOL)willShowOrHideKeyboard;

- (void)keyboardHelperView:(OAKeyboardHelperView*)helperView
           didShowKeyboard:(BOOL)didShowKeyboard;

@end

@interface OAKeyboardHelperView : UIView

@property(nonatomic, weak) id<OAKeyboardHelperViewDelegate> delegat;
@property(nonatomic, assign) BOOL shouldChangeSize;
@property (nonatomic) CGFloat animationDuration;
@property (nonatomic) int animationCurve;

@end
