//
//  CommonUtilities.h
//  test1
//
//  Created by Omar on 11/12/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonUtilities : NSObject

+ (BOOL) isiOS7OrHigher;
+ (UIImage *)imageForColor:(UIColor*)color;
+ (void) addFullSizeLayoutConstraintsForView:(UIView*)innerView
                                      inView:(UIView*)outterView;
+ (BOOL)isArabic;
@end
