//
//  UIImage+OAAdition.h
//  MrContact
//
//  Created by Omar on 12/19/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OAAdition)

- (UIImage *)imageScaledToSize:(CGSize)newSize;
- (NSData*)imageData;

- (void)logImage;
- (void)logImageWithString:(NSString*)string;
+ (UIImage*)imageFromView:(UIView*)view;

@end
