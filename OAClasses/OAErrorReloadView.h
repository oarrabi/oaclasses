//
//  OAErrorReloadView.h
//  Neqaty
//
//  Created by Omar on 12/1/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OAErrorReloadView : UIView
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, copy) void (^reloadBlock)(void);

+ (void)showInView:(UIView*)view
       withMessage:(NSString*)message
       reloadBlock:(void (^)(void))reloadBlock;

+ (void)showInView:(UIView*)view
       withMessage:(NSString*)message
              font:(UIFont*)font
             color:(UIColor*)color
       reloadBlock:(void (^)(void))reloadBlock;

@end
