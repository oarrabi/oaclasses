//
//  NSArray+OAAditions.h
//  Neqaty
//
//  Created by Omar on 12/4/13.
//  Copyright (c) 2013 StartAppz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (OAAditions)
- (BOOL)empty;
@end

@interface NSMutableArray (OAAditions)
- (BOOL)empty;
@end